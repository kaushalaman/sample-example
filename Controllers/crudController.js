/**
 * Created by harekamsingh on 1/13/16.
 */

var async = require('async');
var array = [];
var searchArray = function (queryParams, callbackRoute) {
    //sample code might not work
    async.waterfall([
        function (callback) {
            if (array.indexOf(queryParams.element) !== -1)
                return callback(null, array.indexOf(queryParams.element));

            return callback("element not found");

        }
    ], function (error, result) {
        if (error)
            return callbackRoute(error);

        return callbackRoute(null, "success");
    })
};
var createArray = function () {
//add your logic
};
var sortArray = function () {
//add your logic
};
var deleteArray = function () {
//add your logic
};
module.exports = {
    searchArray: searchArray,
    createArray: createArray,
    sortArray: sortArray,
    deleteArray: deleteArray
};