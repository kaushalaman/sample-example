/**
 * Created by harekamsingh on 1/13/16.
 */

var Joi = require('joi'),
    controller = require('../Controllers');
var CONFIG = require('../Config');
var constants = CONFIG.CONSTANTS;

var getRequest = {
    method: 'GET',
    path: '/api/v1/crud',
    config: {
        description: 'Get request',
        tags: ['api', 'search'],
        handler: function (request, reply) {
            controller.crudController.searchArray(request.query, function (error, success) {
                if (error) {
                    reply(error.response).code(error.statusCode);
                } else {
                    reply(success.response).code(success.statusCode);
                }
            });
        },
        validate: {
            query: {
                element: Joi.number().required()
            }
        },
        response: {
            options: {
                allowUnknown: true
            },
            schema: {
                message: Joi.string().required(),
                data: {
                    index: Joi.number().required()
                }
            }
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: CONFIG.RESPONSE_MESSAGES.SWAGGER_DEFAULT_RESPONSE_MESSAGES
            }
        }

    }
};
var postRequest = {
    method: 'POST',
    path: '/api/v1/crud',
    config: {
        description: 'post request',
        tags: ['api', 'search'],
        handler: function (request, reply) {
            controller.crudController.createArray(request.payload, function (error, success) {
                if (error) {
                    reply(error.response).code(error.statusCode);
                } else {
                    reply(success.response).code(success.statusCode);
                }
            });
        },
        validate: {
            payload: {
                array: Joi.array().items(Joi.number().required()).required()
            }
        },
        response: {
            options: {
                allowUnknown: true
            },
            schema: {
                message: Joi.string().required(),
                data: {}
            }
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: CONFIG.RESPONSE_MESSAGES.SWAGGER_DEFAULT_RESPONSE_MESSAGES
            }
        }

    }
};
var putRequest = {
    method: 'PUT',
    path: '/api/v1/crud',
    config: {
        description: 'put request',
        tags: ['api', 'search'],
        handler: function (request, reply) {
            controller.crudController.sortArray(request.payload, function (error, success) {
                if (error) {
                    reply(error.response).code(error.statusCode);
                } else {
                    reply(success.response).code(success.statusCode);
                }
            });
        },
        validate: {
            payload: {
                element: Joi.number().required()
            }
        },
        response: {
            options: {
                allowUnknown: true
            },
            schema: {
                message: Joi.string().required(),
                data: {}
            }
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: CONFIG.RESPONSE_MESSAGES.SWAGGER_DEFAULT_RESPONSE_MESSAGES
            }
        }

    }
};
var deleteRequest = {
    method: 'DELETE',
    path: '/api/v1/crud/{element}',
    config: {
        description: 'delete request',
        tags: ['api', 'search'],
        handler: function (request, reply) {
            controller.crudController.deleteArray(request.params, function (error, success) {
                if (error) {
                    reply(error.response).code(error.statusCode);
                } else {
                    reply(success.response).code(success.statusCode);
                }
            });
        },
        validate: {
            params: {
                element: Joi.number().required()
            }
        },
        response: {
            options: {
                allowUnknown: true
            },
            schema: {
                message: Joi.string().required(),
                data: {
                    index: Joi.number().required()
                }
            }
        },
        plugins: {
            'hapi-swagger': {
                responseMessages: CONFIG.RESPONSE_MESSAGES.SWAGGER_DEFAULT_RESPONSE_MESSAGES
            }
        }

    }
};

module.exports = [
    getRequest,
    postRequest,
    putRequest,
    deleteRequest
];