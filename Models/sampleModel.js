/**
 * Created by harekamsingh on 1/31/16.
 */
var mongoose = require('mongoose');
var constants = require('../Config/constants');
var Schema = mongoose.Schema;
var friends = new Schema({
    id: {type: Number},
    name: {type: String}
}, {_id: false});
var sampleOne = new Schema({
    index: {type: Number},
    guid: {type: String},
    isActive: {type: Boolean},
    balance: {type: Number},
    picture: {type: String},
    age: {type: Number},
    eyeColor: {type: String, enum: [constants.EYE_COLOR.blue, constants.EYE_COLOR.brown, constants.EYE_COLOR.green]},
    name: {type: String},
    gender: {type: String},
    company: {type: String},
    email: {type: String},
    phone: {type: String},
    address: {type: String},
    about: {type: String},
    registered: {type: Date},
    location: {
        'type': {type: String, enum: constants.GEO_JSON_TYPES.Point, default: constants.GEO_JSON_TYPES.Point},
        coordinates: {type: [Number], default: [0, 0]}
    },
    //latitude: '{{floating(-90.000001, 90)}}',
    //longitude: '{{floating(-180.000001, 180)}}',
    tags: {type: Array},
    friends: [friends],
    greeting: {type: String},
    favoriteFruit: {type: String, enum: [constants.FRUITS.apple, constants.FRUITS.strawberry, constants.FRUITS.banana]}

});

module.exports = mongoose.model('sampleOne', sampleOne);